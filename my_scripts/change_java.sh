#!/usr/bin/env bash

export JAVA_HOME="/home/anotni/.jdks/graalvm-ce-17/"
export PATH=$JAVA_HOME/bin:$PATH
# have to change GRADLE_HOME as well
# export GRADLE_HOME=jdk-install-dir
# export PATH=$JAVA_HOME/bin:$PATH
