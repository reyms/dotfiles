#!/usr/bin/bash

usage() {
    echo "Usage: trash_files [PATTERN]"
    exit
}

if [ $# -lt 1 ]; then
    usage
fi

echo "trash files containing $1 in their name"

for f in $(fd --type f "$1"); do
    trash "$f"
done
