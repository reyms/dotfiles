(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#ffffff" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e"])
 '(custom-safe-themes
   '("a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "3f1dcd824a683e0ab194b3a1daac18a923eed4dba5269eecb050c718ab4d5a26" "4f1d2476c290eaa5d9ab9d13b60f2c0f1c8fa7703596fa91b235db7f99a9441b" "5784d048e5a985627520beb8a101561b502a191b52fa401139f4dd20acb07607" "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "745d03d647c4b118f671c49214420639cb3af7152e81f132478ed1c649d4597d" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" default))
 '(exwm-floating-border-color "#c5c8c6")
 '(fci-rule-color "#c5c8c6")
 '(highlight-tail-colors ((("#e9e9ea") . 0) (("#e9e9ea") . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#969896" "#444444"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#969896" "#282a2e"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#969896" "#282a2e"))
 '(objed-cursor-color "#282a2e")
 '(pdf-view-midnight-colors (cons "#282a2e" "#ffffff"))
 '(rustic-ansi-faces
   ["#ffffff" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e" "#282a2e"])
 '(safe-local-variable-values
   '((vc-prepare-patches-separately)
     (diff-add-log-use-relative-names . t)
     (vc-git-annotate-switches . "-w")))
 '(vc-annotate-background "#ffffff")
 '(vc-annotate-color-map
   (list
    (cons 20 "#282a2e")
    (cons 40 "#282a2e")
    (cons 60 "#282a2e")
    (cons 80 "#282a2e")
    (cons 100 "#282a2e")
    (cons 120 "#282a2e")
    (cons 140 "#282a2e")
    (cons 160 "#282a2e")
    (cons 180 "#282a2e")
    (cons 200 "#282a2e")
    (cons 220 "#282a2e")
    (cons 240 "#282a2e")
    (cons 260 "#282a2e")
    (cons 280 "#282a2e")
    (cons 300 "#282a2e")
    (cons 320 "#282a2e")
    (cons 340 "#c5c8c6")
    (cons 360 "#c5c8c6")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-types '((lsp-mode) (defvaralias))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'projectile-ripgrep 'disabled nil)
