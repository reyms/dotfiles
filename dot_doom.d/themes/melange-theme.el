;;; themes/melange-theme.el -*- lexical-binding: t; no-byte-compile:t; -*-
;;
;; Author:
;; Maintainer:
;; Source:
;;; Commentary:
;;; Code:

(require 'doom-themes)


;;
;;; Variables

(defgroup melange-theme nil
  "Options for the `melange' theme."
  :group 'my-custom-themes)

(defcustom melange-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'melange
  :type 'boolean)

(defcustom melange-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'melange
  :type 'boolean)

(defcustom melange-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'melange
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme melange
  "A dark theme where data is colder"

  ;; name        default   256           16
  ((bg         '("#2A2520" "black"       "black"  ))
   (fg         '("#ECE1D7" "#bfbfbf"     "brightwhite"  ))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#21242b" "black"       "black"        ))
   (fg-alt     '("#5B6268" "#2d2d2d"     "white"        ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#2A2520" "black"       "black"        ))
   (base1      '("#352F2A" "#1e1e1e"     "brightblack"  ))
   (base2      '("#202328" "#2e2e2e"     "brightblack"  ))
   (base3      '("#23272e" "#262626"     "brightblack"  ))
   (base4      '("#4D453E" "#3f3f3f"     "brightblack"  ))
   (base5      '("#5B6268" "#525252"     "brightblack"  ))
   (base6      '("#A38D78" "#6b6b6b"     "brightblack"  ))
   (base7      '("#C1A78E" "#979797"     "brightblack"  ))
   (base8      '("#ECE1D7" "#dfdfdf"     "white"        ))

   (grey       base4)
   (red        '("#B65C60" "#B65C60" "red"          ))
   (orange     '("#F17C64" "#F17C64" "brightred"    ))
   (green      '("#78997A" "#78997A" "green"        ))
   (teal       '("#99D59D" "#99D59D" "brightgreen"  ))
   (yellow     '("#EBC06D" "#EBC06D" "yellow"       ))
   (blue       '("#697893" "#9AACCE" "brightblue"   ))
   (dark-blue  '("#697893" "#697893" "blue"         ))
   (magenta    '("#B380B0" "#B380B0" "brightmagenta"))
   (violet     '("#a9a1e1" "#a9a1e1" "magenta"      ))
   (cyan       '("#88B3B2" "#88B3B2" "brightcyan"   ))
   (dark-cyan  '("#86A3A3" "#86A3A3" "cyan"         )))

   ;; ;; [palette]
   ;; # Grays
   ;; (bg             '("#2A2520"))
   ;; (overbg         '("#352F2A"))
   ;; (sel            '("#4D453E"))
   ;; (com            '("#A38D78"))
   ;; (faded          '("#C1A78E"))
   ;; (fg             '("#ECE1D7"))
   ;; ;; # Bright
   ;; (bright_red     '("#F17C64"))
   ;; (bright_yellow  '("#EBC06D"))
   ;; (bright_green   '("#99D59D"))
   ;; (bright_cyan    '("#88B3B2"))
   ;; (bright_blue    '("#9AACCE"))
   ;; (bright_magenta '("#CE9BCB"))
   ;; ;; # "regular" colours
   ;; (red            '("#B65C60"))
   ;; (yellow         '("#E49B5D"))
   ;; (green          '("#78997A"))
   ;; (cyan           '("#86A3A3"))
   ;; (blue           '("#697893"))
   ;; (magenta        '("#B380B0"))
   ;; ;; # dark colours (mainly for backgournds)
   ;; (dark_red       '("#7D2A2F"))
   ;; (dark_yellow    '("#8E733F"))
   ;; (dark_green     '("#1F3521"))
   ;; (dark_cyan      '("#213433"))
   ;; (dark_blue      '("#243146"))
   ;; (dark_magenta   '("#462445"))

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      blue)
   (vertical-bar   (doom-darken sel 0.1))
   (selection      dark-blue)
   (builtin        magenta)
   (comments       (if melange-brighter-comments faded com))
   (doc-comments   (doom-lighten (if melange-brighter-comments faded com) 0.25))
   (constants      magenta)
   (functions      yellow)
   (keywords       orange)
   (methods        cyan)
   (operators      blue)
   (type           green)
   (strings        blue)
   (variables      fg)
   (numbers        orange)
   (region         `(,(doom-lighten (car bg) 0.15) ,@(doom-lighten (cdr sel) 0.35)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; ;; These are extra color variables used only in this theme; i.e. they aren't
   ;; ;; mandatory for derived themes.
   ;; (modeline-fg              fg)
   ;; (modeline-fg-alt          faded)
   ;; (modeline-bg              (if melange-brighter-modeline
   ;;                               (doom-darken blue 0.45)
   ;;                             (doom-darken overbg 0.1)))
   ;; (modeline-bg-alt          (if melange-brighter-modeline
   ;;                               (doom-darken blue 0.475)
   ;;                             `(,(doom-darken (car bg) 0.15) ,@(cdr bg))))
   ;; (modeline-bg-inactive     `(,(car bg-alt) ,@(cdr base1)))
   ;; (modeline-bg-inactive-alt `(,(doom-darken (car overbg) 0.1) ,@(cdr bg)))
   ;;
   ;; (-modeline-pad
   ;;  (when melange-padded-modeline
   ;;    (if (integerp melange-padded-modeline) melange-padded-modeline 4))))


  ;;;; Base theme face overrides
  ;; (((line-number &override) :foreground faded)
  ;;  ((line-number-current-line &override) :foreground fg)
  ;;  ((font-lock-comment-face &override)
  ;;   :background (if melange-brighter-comments (doom-lighten bg 0.05)))
  ;;  (mode-line
  ;;   :background modeline-bg :foreground modeline-fg
  ;;   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
  ;;  (mode-line-inactive
  ;;   :background modeline-bg-inactive :foreground modeline-fg-alt
  ;;   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
  ;;  (mode-line-emphasis :foreground (if melange-brighter-modeline fg highlight))

  ;;  ;;;; css-mode <built-in> / scss-mode
  ;;  (css-proprietary-property :foreground orange)
  ;;  (css-property             :foreground green)
  ;;  (css-selector             :foreground blue)
  ;;  ;;;; doom-modeline
  ;;  (doom-modeline-bar :background (if melange-brighter-modeline modeline-bg highlight))
  ;;  (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
  ;;  (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
  ;;  (doom-modeline-buffer-project-root :foreground green :weight 'bold)
  ;;  ;;;; elscreen
  ;;  (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
  ;;  ;;;; ivy
  ;;  (ivy-current-match :background dark-blue :distant-foreground base0 :weight 'normal)
  ;;  ;;;; LaTeX-mode
  ;;  (font-latex-math-face :foreground green)
  ;;  ;;;; markdown-mode
  ;;  (markdown-markup-face :foreground faded)
  ;;  (markdown-header-face :inherit 'bold :foreground red)
  ;;  ((markdown-code-face &override) :background (doom-lighten com 0.05))
  ;;  ;;;; rjsx-mode
  ;;  (rjsx-tag :foreground red)
  ;;  (rjsx-attr :foreground orange)
  ;;  ;;;; solaire-mode
  ;;  (solaire-mode-line-face
  ;;   :inherit 'mode-line
  ;;   :background modeline-bg-alt
  ;;   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
  ;;  (solaire-mode-line-inactive-face
  ;;   :inherit 'mode-line-inactive
  ;;   :background modeline-bg-inactive-alt
  ;;   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt))))

  ;;;; Base theme variable overrides-
  ()
  )

;;; themes/melange-theme.el ends here
