;;; my-autumn-theme.el --- -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Date: 24 March, 2023
;; Author:
;; Maintainer:
;; Source: https://github.com/Plunne/doom-feather-theme
;;
;;; Code:

(require 'doom-themes)

;;
;;; Variables

(defgroup my-autumn-theme nil
  "Options for the `my-feather' theme."
  :group 'my-custom-themes)

(defcustom doom-feather-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'my-autumn-theme
  :type 'boolean)

(defcustom doom-feather-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'my-autumn-theme
  :type 'boolean)

(defcustom doom-feather-dark-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'my-autumn-theme
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme my-autumn
  "A dark theme based on Doom One Dark."

  ;; name        default   256           16
  ((bg         '("#262626" "#262626"    "black"                         ))
   (fg         '("#F3F2CC" "#F3F2CC"    "brightwhite"   ))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#323232" "#323232"    "black"                         ))
   (fg-alt     '("#F3F2CC" "#F3F2CC"    "white"                         ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#262626" "#262626"    "black"         ))
   (base1      '("#2b2b2b" "#2b2b2b"    "brightblack"   ))
   (base2      '("#323232" "#323232"    "brightblack"   ))
   (base3      '("#404040" "#404040"    "brightblack"   ))
   (base4      '("#6a6a6a" "#6a6a6a"    "brightblack"   ))
   (base5      '("#848484" "#848484"    "brightblack"   ))
   (base6      '("#a8a8a8" "#a8a8a8"    "brightblack"   ))
   (base7      '("#c8c8c8" "#c8c8c8"    "brightblack"   ))
   (base8      '("#e8e8e8" "#e8e8e8"    "white"         ))

   ;; can't rename those
   (grey       base4)
   (red        '("#F05E48" "#F05E48"    "red"           ))
   (orange     '("#cfba8b" "#cfba8b"    "brightred"     ))
   (green      '("#99be70" "#99be70"    "green"         ))
   (teal       '("#86c1b9" "#86c1b9"    "brightgreen"   ))
   (yellow     '("#FAD566" "#FAD566"    "yellow"        ))
   (blue       '("#51afef" "#00afff"    "brightblue"    ))
   (dark-blue  '("#2257A0" "#005faf"    "blue"          ))
   (magenta    '("#7A619A" "#875faf"    "brightmagenta" ))
   (violet     '("#9783b1" "#8787af"    "magenta"       ))
   (cyan       '("#86c1b9" "#86c1b9"    "brightcyan"    ))
   (dark-cyan  '("#72a59e" "#72a59e"    "cyan"          ))

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      base5)
   (vertical-bar   (doom-darken base1 0.1))
   (selection      dark-blue)
   (builtin        orange)
   (comments       (if doom-feather-brighter-comments base7 base5))
   (doc-comments   (doom-lighten (if doom-feather-brighter-comments base7 base5) 0.25))
   (constants      base8)
   (functions      yellow)
   (keywords       red)
   (methods        yellow)
   (operators      base8)
   (type           fg)
   (strings        green)
   (variables      fg)
   (numbers        cyan)
   (region         `(,(doom-lighten (car bg-alt) 0.15) ,@(doom-lighten (cdr base1) 0.35)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; These are extra color variables used only in this theme; i.e. they aren't
   ;; mandatory for derived themes.
   (modeline-fg              base6)
   (modeline-fg-alt          base4)
   (modeline-bg              (if doom-feather-brighter-modeline
                                 (doom-darken blue 0.45)
                               bg-alt))
   (modeline-bg-alt          (if doom-feather-brighter-modeline
                                 (doom-darken blue 0.475)
                               `(,(doom-darken (car bg-alt) 0.15) ,@(cdr bg))))
   (modeline-bg-inactive     `(,(car bg-alt) ,@(cdr base1)))
   (modeline-bg-inactive-alt `(,(doom-darken (car bg-alt) 0.1) ,@(cdr bg)))

   (-modeline-pad
    (when doom-feather-dark-padded-modeline
      (if (integerp doom-feather-dark-padded-modeline) doom-feather-dark-padded-modeline 4))))


  ;;;; Base theme face overrides
  (;((line-number &override) :foreground base5)
   ((line-number-current-line &override) :background (doom-lighten bg 0.00) :foreground base2 :weight 'bold)
   ((font-lock-comment-face &override)
    :background (if doom-feather-brighter-comments (doom-lighten bg 0.05)) :italic t)
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if doom-feather-brighter-modeline base8 highlight))
   ;;;; button (#include "strings")
   (button :foreground strings)
   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; dashboard
   (dashboard-navigator :foreground teal)
   ;;;; doom-modeline
   (doom-modeline-bar :background (if doom-feather-brighter-modeline modeline-bg highlight))
   (doom-modeline-buffer-file :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; ivy
   (ivy-current-match :background base7 :distant-foreground bg-alt :weight 'normal)
   ;;;; LaTeX-mode
   (font-latex-math-face :foreground green)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground yellow)
   ((markdown-code-face &override) :background base3)
   ;;;; Outlines
   ;; (outline-1 :height 1.8 :foreground blue :weight 'bold)
   ;; (outline-2 :height 1.2 :foreground magenta :weight 'bold)
   ;; (outline-3 :height 1.1 :foreground violet :weight 'bold)
   ;; (outline-4 :height 1.0 :foreground (doom-lighten blue 0.25) :weight 'bold)
   ;; (outline-5 :height 1.0 :foreground (doom-lighten magenta 0.25) :weight 'bold)
   ;; (outline-6 :height 1.0 :foreground (doom-lighten blue 0.5) :weight 'bold)
   ;; (outline-7 :height 1.0 :foreground (doom-lighten magenta 0.5) :weight 'bold)
   ;; (outline-8 :height 1.0 :foreground (doom-lighten blue 0.8) :weight 'bold)
   ;;;; rainbow-delimiters
   ;; (rainbow-delimiters-depth-1-face :foreground base6)
   ;; (rainbow-delimiters-depth-2-face :foreground yellow)
   ;; (rainbow-delimiters-depth-3-face :foreground orange)
   ;; (rainbow-delimiters-depth-4-face :foreground blue)
   ;; (rainbow-delimiters-depth-5-face :foreground green)
   ;; (rainbow-delimiters-depth-6-face :foreground red)
   ;; (rainbow-delimiters-depth-7-face :foreground cyan)
   ;; (rainbow-delimiters-depth-8-face :foreground teal)
   ;; (rainbow-delimiters-depth-9-face :foreground yellow)
   ;;;; Treemacs
   ;; (treemacs-root-face :foreground teal :weight 'bold :height 1.4)
   ;; (doom-themes-treemacs-root-face :foreground teal :weight 'ultra-bold :height 1.2)
   ;;;; Tree-sitter
   (tree-sitter-hl-face:punctuation.bracket :foreground comments)
   (tree-sitter-hl-face:attribute :foreground blue)
   (tree-sitter-hl-face:function\.call :foreground yellow)
   (tree-sitter-hl-face:function\.macro :foreground orange)
   (tree-sitter-hl-face:type\.builtin :foreground teal :italic t)
   (tree-sitter-hl-face:variable\.special :foreground variables)
   (tree-sitter-hl-face:operator :foreground operators)
   (tree-sitter-hl-face:property :foreground orange :italic nil)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt))))

  ;;;; Base theme variable overrides
  ())

;;; my-autumn-theme.el ends here
