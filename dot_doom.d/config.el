;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "antoni"
      user-mail-address "reym@veril.mozmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq my-font "Noto Sans")
(if (string-equal (system-name) "antoni-surfacebook")
    (setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 28)
      ;; (setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 14)
            doom-variable-pitch-font (font-spec :family my-font :size 40))
    (setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 16)
          doom-variable-pitch-font (font-spec :family my-font)))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!
;;
;; (setq doom-font (font-spec :family "Azeret Mono" :size 14 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-gruvbox)
;; (setq doom-theme 'gruber-darker)
;; (setq doom-theme 'doom-material-dark)
(setq doom-theme 'my-autumn)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


;;; Personal Config

(setq! doom-gruvbox-dark-variant "hard")
;; (setq! doom-molokai-brighter-comments t)

(setq! +zen-text-scale 1)  ; zenmode text scale

(setq! confirm-kill-emacs nil) ; just quit emacs

;; (setq! ranger-override-dired 'ranger)           ; Make dired not start in minimal (deer) mode
;; (after! ranger (ranger-override-dired-mode +1))

;; (toggle-scroll-bar t)
(setq! select-enable-clipboard nil)

;;; modeline settings
;; (setq! doom-modeline-modal-icon t)
;; (setq! doom-modeline-continuous-word-count-modes `(markdown-mode gfm-mode org-mode))

(setq! highlight-indent-guides-responsive 'top)

(defun my-highlighter (level responsive display)
  (if (> 1 level)
      nil
    (highlight-indent-guides--highlighter-default level responsive display)))

(setq highlight-indent-guides-highlighter-function 'my-highlighter)

;;; to make backupfiles not be in the same dir as source-code
(setq! backup-directory-alist `(("." . "~/.saves")))

(setq make-backup-files t)

;; Markdown Config
(setq! markdown-hide-markup t)

;; Python
(after! dap-mode
  (setq dap-python-debugger 'debugpy))

;;; Keymap
;; get key-code: SPC-h-c
(map! :n "M-h" #'evil-window-left
      :n "M-l" #'evil-window-right
      :n "M-j" #'evil-window-down
      :n "M-k" #'evil-window-up
      :g "C-M-d" #'org-roam-dailies-goto-today
      :n "g l" #'evil-end-of-line
      :n "g h" #'evil-beginning-of-line)

;; (map! :v "SPC y" #'kill-ring-save)

;; Emacs doesn't play well with fish, nushell etc
(setq shell-file-name "/bin/bash")

;; rss reader elfeed org file
(setq rmh-elfeed-org-files '("/home/antoni/org/rss-feeds.org"))

(add-hook 'org-mode-hook 'mixed-pitch-mode) ; start mixed-pitched-mode when org is active
(add-hook 'markdown-mode-hook 'mixed-pitch-mode)

(add-hook 'helpful-mode-hook 'mixed-pitch-mode)
(add-hook 'Info-mode-hook 'mixed-pitch-mode)
(add-hook 'help-mode-hook 'mixed-pitch-mode)
(add-hook 'lsp-help-mode-hook '(mixed-pitch-mode))

;; (setq modus-themes-common-palette-overrides modus-themes-preset-overrides-faint)
;; (setq modus-themes-common-palette-overrides '((border-mode-line-active unspecified)
;;                                               (border-mode-line-inactive unspecified)))

;;; config.el ends here
