# Nushell Environment Config File

def my_right_prompt [] {
    date now | format date "%H:%M"
}
$env.PROMPT_COMMAND_RIGHT = (my_right_prompt)

$env.ENV_CONVERSIONS = {
  "PATH": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  "Path": {
    from_string: { |s| $s | split row (char esep) | path expand -n }
    to_string: { |v| $v | path expand -n | str join (char esep) }
  }
  # "GUIX_PROFILE": {
    # from_string: { |s| $s | parse "{variable}={path}" }
    # to_string: { |v| $"GUIX_PROFILE=(char double_quote)($v)(char double_quote)" }
    # to_string: { |v| $v }
  # }
}

# Directories to search for scripts when calling source or use
#
# By default, <nushell-config-dir>/scripts is added
$env.NU_LIB_DIRS = [
    ($nu.config-path | path dirname | path join 'scripts')
    # ($env.HOME | path join 'Code/Nu-Code/nu_scripts')
]

# Directories to search for plugin binaries when calling register
#
# By default, <nushell-config-dir>/plugins is added
$env.NU_PLUGIN_DIRS = [
    ($nu.config-path | path dirname | path join 'plugins')
]

$env.PATH = ($env.PATH | split row (char esep)
    | append '/home/antoni/.local/bin' # /usr/local/bin ist schon im path aber das ist ein anderer pfad
    | prepend '/home/antoni/.cargo/bin/'
    | prepend '/home/antoni/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin/'
    | append '/home/antoni/.roswell/bin/'
    | append '/home/antoni/.nix-profile/bin'
    | append '/nix/var/nix/profiles/default/bin'
    # | append '/home/antoni/anaconda3/bin/'
    | append '/home/antoni/my_scripts')

$env.EDITOR = "/home/antoni/.cargo/bin/hx"
$env.VISUAL = "/home/antoni/.cargo/bin/hx"

# source "/home/antoni/Code/Nu-Code/nu_scripts/prompt/starship.nu"
# source ~/.cache/starship/init.nu
starship init nu | str replace 'let-env ' '$env.' -an | save -f ~/.cache/starship/init.nu
# starship init nu | print $in
source ~/.cache/starship/init.nu

# conda vitual envs
# source "/home/antoni/Code/Nu-Code/nu_scripts/virtual_environments/"
# this doesn't work but when you go into another shell and change the env there and then start 
# nushell inside it, the env is copied over...
# use /home/antoni/Code/Nu-Code/u_scripts/virtual_environments/nu_conda/nu_conda.nu

##### TODO add guix properly
# Alternately, see `guix package --search-paths -p "/home/antoni/.config/guix/current"'.
$env.GUIX_PROFILE = "/home/antoni/.config/guix/current"
$env.GUIX_LOCPATH = "/home/antoni/.guix-profile/lib/locale"
# $env.PATH = ($env.PATH | split row (char esep) 
#     | append $"($env.GUIX_PROFILE)/etc/profile")
     # . "$GUIX_PROFILE/etc/profile" # (period) command is synonym to source

# don't understand why this doesn't work
# => it wasn't working, because of a newline at the end
$env.PATH = ($env.PATH
                | split row (char esep)
                # | append (guix package --search-paths -p .guix-profile
                #           | parse "export PATH={path_var}"
                #           | get path_var
                #           | echo $in
                #           | str replace -a '"' ''
                #           | str trim -c (char newline))
                | append (guix package --search-paths -p "/home/antoni/.config/guix/current"
                          | parse "export PATH={path_var}"
                          | get path_var
                          | echo $in
                          | str replace -a '"' ''
                          | str trim -c (char newline)))

### ghcup
if not ("/home/antoni/.ghcup/bin" in $env.PATH) {
    $env.PATH = ($env.PATH | split row (char esep)
    | append $"/home/antoni/.ghcup/bin")
} 
if not ("/home/antoni/.cabal/bin" in $env.PATH) {
    $env.PATH = ($env.PATH | split row (char esep)
    | append $"/home/antoni/.cabal/bin")
} 
if not ("/home/antoni/.ghcup/hls" in $env.PATH) {
    $env.PATH = ($env.PATH | split row (char esep)
    | append $"/home/antoni/.ghcup/hls")
} 

### carp-lang

$env.CARP_DIR = "/home/antoni/Code/Haskell-Code/Carp"


### opam / ocaml
# opam init
# (opam env)
#
# command that I crafted...
# opam env | split row ";"
# | str replace (char newline) ''
# | str trim
# | where not ($it | str starts-with "export")
# | where not ($it | is-empty)
# | split column
# 
# command form nushell github
opam env | str trim | lines
    | each {|a| $a | split column ';' var export}
    | each {|b| $b.var | split column '=' name path}
    | flatten
    | str trim path -c "'"
    | echo $in
    | transpose -ird
    | load-env

### Volta (javascript tooling)

$env.VOLTA_HOME = '/home/antoni/.volta'
$env.PATH = ($env.PATH | split row (char esep)
    | prepend $"($env.VOLTA_HOME)/bin")

# dotnet (c#/csharp)

$env.DOTNET_CLI_TELEMETRY_OPTOUT = 1

# Julia

$env.JULIA_SHELL = $"(which bash | get path | to text)"
