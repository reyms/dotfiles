{ pkgs, config, ... }: {
  programs.lf = {
    enable = true;

    # define custom commands
    commands = {
      dragon-out = ''${pkgs.xdragon}/bin/xdragon -a -x "$fx"'';
      editor-open = "$$EDITOR $f";
      open-with = ''
        ''${{
            clear
            set -f
            rifle -l $fx | sed -e "s/:[a-Z]*:[a-Z]*:/ \| /"
            read -p "open with: " method
            rifle -p $method $fx
        }}
        map r open-with
      '';
    };

    keybindings = {
      O = "open-with";
    };

    settings = {
      preview = true;
  # to download the icons file do:
  # nix run nixpkgs#wget -- "https://raw.githubusercontent.com/gokcehan/lf/master/etc/icons.example" -O ~/.config/lf/icons
      icons = true;
      ignorecase = true;
    };

  };
}
