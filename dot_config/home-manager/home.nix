{ config, pkgs, gBar, ... }:

{
  home.username = "antoni";
  home.homeDirectory = "/home/antoni";

  home.stateVersion = "23.05"; # protect against breaking versions of hm

  home.packages = with pkgs; [
    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    fd
    bat

    ncdu # TUI disk usage
    timer # To help with my ADHD paralysis (from misterio config)

    nil # Nix LSP
    nixfmt # Nix formatter

    # python3.pkgs.ipython

    hexyl # hex viewer

    taplo # toml lsp

    distrobox
  ];

  # manage plain files
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  # You can also manage environment variables but you will have to manually
  # source
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  # or
  #  /etc/profiles/per-user/antoni/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
  };

  imports = [
    gBar.homeManagerModules.x86_64-linux.default
    ./lf.nix
  ];

  # has weird persmission issues and system wide install just works 
  # services.syncthing = {
  #   enable = true;
  #   tray.enable = true;
  # };

  services.mako = {
    enable = true;
    defaultTimeout = 8000;
  };

  services.darkman = {
    enable = true;
    settings = {
      lat = 53.551086;
      lon = 9.993682;
      # usegeoclue = true;
    };
  };

  services.wlsunset = {
    enable = true;
    latitude = "53.551086";
    longitude = "9.993682";
    temperature.day = 6500;
    temperature.night = 4000;
  };



  programs.git = {
    enable = true;
    # package = pkgs.gitAndTools.gitFull;
    # userName = "antoni";
    # userEmail = "a.reym@proton.me";
    lfs.enable = true;
    ignores = [ ".direnv" "result" ];
  };

  programs.lazygit.enable = true;
  programs.bottom.enable = true;
  programs.broot = {
    enable = true;
    settings = {
      modal = true;
    };
  };

  # graphical programs

  programs.zathura = {
    enable = true;
    # TODO migrate config
    # - get config settings from file
    # - remove file from chezmoi
    # - remove file from git repo
    extraConfig = ''
      set selection-clipboard clipboard
      "unmap Left
      "unmap Right
      map <Left> navigate previous
      map <Right> navigate next
                  '';
  };

  programs.gBar = {
     enable = true;
     config = {
       Location = "T";
       EnableSNI = true;
       SNIIconSize = {
         Discord = 26;
         OBS = 23;
       };
       WorkspaceSymbols = [ " " " " ];
        };
  };

  programs.home-manager.enable = true; # Let Home Manager install/manage itself.
}
