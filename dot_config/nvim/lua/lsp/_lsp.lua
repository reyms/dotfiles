-- main lsp config

local mason = require('mason')
local lspconfig = require('lspconfig')
local util = require('vim.lsp.util')

-- 1. Set up nvim-lsp-installer first!
mason.setup({})

local function on_attach()
	vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = 0 }) -- no parens means it's a function-ref and I'm not calling it here
	vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
	vim.keymap.set('n', 'gt', vim.lsp.buf.type_definition, { buffer = 0 })
	vim.keymap.set('n', 'gD', vim.lsp.buf.implementation, { buffer = 0 })
	vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_next, { buffer = 0 })
	vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_prev, { buffer = 0 })
	vim.keymap.set('n', '<leader>dl', '<cmd>Telescope diagnostics<cr>', { buffer = 0 }) -- use Ctrl + q to add to quick-fix list
	vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, { buffer = 0 })
	vim.keymap.set('n', '<M-CR>', vim.lsp.buf.code_action, { buffer = 0 })
end

-- 2. (optional) Override the default configuration to be applied to all servers.
lspconfig.util.default_config = vim.tbl_extend('force', lspconfig.util.default_config, {
	on_attach = on_attach,
})

-- 3. Loop through all of the installed servers and set it up via lspconfig
for _, server in ipairs(lsp_installer.get_installed_servers()) do
	lspconfig[server.name].setup({})
end

local formatting_callback = function(client, bufnr)
	vim.keymap.set('n', '<leader>f', function()
		local params = util.make_formatting_params({})
		client.request('textDocument/formatting', params, nil, bufnr)
	end, { buffer = bufnr })
end

-- lspconfig.rust_analyzer.setup({
-- 	on_attach = function()
-- 		vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = 0 })
-- 		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
-- 	end,
-- })

-- lspconfig.elmls.setup({
-- 	on_attach = function()
-- 		vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = 0 })
-- 		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
-- 	end,
-- })

lspconfig.sumneko_lua.setup({
	on_attach = function()
		vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = 0 }) -- no parens means it's a function-ref and I'm not calling it here
		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
		vim.keymap.set('n', 'gt', vim.lsp.buf.type_definition, { buffer = 0 })
		vim.keymap.set('n', 'gD', vim.lsp.buf.implementation, { buffer = 0 })
		vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_next, { buffer = 0 })
		vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_prev, { buffer = 0 })
		vim.keymap.set('n', '<leader>dl', '<cmd>Telescope diagnostics<cr>', { buffer = 0 }) -- use Ctrl + q to add to quick-fix list
		vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, { buffer = 0 })
		vim.keymap.set('n', '<M-CR>', vim.lsp.buf.code_action, { buffer = 0 })
	end,

	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = 'LuaJIT',
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { 'vim' },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file('', true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
	},
})

lspconfig.bashls.setup({
	on_attach = function()
		vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = 0 })
		vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = 0 })
	end,
})
