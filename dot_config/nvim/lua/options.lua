vim.o.hlsearch = true            -- Set highlight on search
vim.wo.number = true              -- Make line numbers default
vim.o.mouse = 'a'                 -- Enable mouse mode
vim.o.clipboard = 'unnamedplus'   -- sync clipboard
-- vim.o.breakindent = true          -- indent wrapped lines vim.o.undofile = true             -- Save undo history

-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

vim.wo.signcolumn = 'yes' -- show icons on the left

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set colorscheme
vim.o.termguicolors = true
vim.cmd [[colorscheme melange]]

vim.o.conceallevel = 2
