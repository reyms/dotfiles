-- coc-config: coc specific keybinds
local keymap = vim.api.nvim_set_keymap
keymap('n', '<M-CR>', '<Plug>(coc-codeaction)', {})
keymap('n', 'gd', '<Plug>(coc-definition)', { silent = true })
keymap('n', 'K', ":call CocActionAsync('doHover')<CR>", { silent = true, noremap = true })
keymap('n', '<leader>rn', '<Plug>(coc-rename)', {})
keymap('i', '<C-Space>', 'coc#refresh()', { silent = true, expr = true })

keymap('i', '<TAB>', "coc#pum#visible() ? '<C-n>' : '<TAB>'", { noremap = true, silent = true, expr = true })

keymap('i', '<S-TAB>', "coc#pum#visible() ? '<C-p>' : '<C-h>'", { noremap = true, expr = true })
keymap(
	'i',
	'<CR>',
	"pumvisible() ? coc#_select_confirm() : '<C-G>u<CR><C-R>=coc#on_enter()<CR>'",
	{ silent = true, expr = true, noremap = true }
)
vim.o.hidden = true
vim.o.backup = false
vim.o.writebackup = false
vim.o.updatetime = 300
-- TODO: add installed extensions here so that i don't always have to install them manually
