-- telescope config
-- local status_ok, telescope = pcall(require("telescope"))
-- if not status_ok then
--   print("telescope failed to load")
--   return
-- end
local telescope = require('telescope')

telescope.setup({
	pickers = {
		find_files = {
			theme = 'ivy',
		},
	},
})

-- telescope.load_extension('git_worktree')

local keymap = vim.api.nvim_set_keymap

keymap('n', '<C-P>', "<cmd>lua require('telescope.builtin').find_files()<CR>", { noremap = true })
keymap('n', '<C-M-F>', "<cmd>lua require('telescope.builtin').live_grep()<CR>", { noremap = true })
keymap('n', '<leader>bb', "<cmd>lua require('telescope.builtin').buffers()<CR>", { noremap = true })
keymap('n', '<C-T>', "<cmd>lua require('telescope').extensions.git_worktree.git_worktrees()<CR>", { noremap = true })
