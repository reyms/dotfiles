local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
		vim.cmd([[packadd packer.nvim]])
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

-- autocmd to reload nvim when plugins.lua gets saved
-- vim.cmd([[
--   augroup packer_user_config
--     autocmd!
--     autocmd BufWritePost plugins.lua source <afile> | PackerSync
--   augroup end
-- ]])

return require('packer').startup(function(use)
	use('wbthomason/packer.nvim') -- Packer can manage itself
	-- use({ 'neoclide/coc.nvim', branch = 'release' }) -- autocompletion and errors

	use({
		'nvim-lualine/lualine.nvim',
		Requires = { 'kyazdani42/nvim-web-devicons', 'arkav/lualine-lsp-progress', opt = true },
		config = function()
			require('plugins.line-config')
		end,
	})

	use({
		'nvim-treesitter/nvim-treesitter',
		config = function()
			require('nvim-treesitter.configs').setup({
				context_commentstring = {
					enable = true,
				},
				highlight = {
					enable = true,
				},
				indent = {
					enable = true,
				},
			})
		end,
	})

	use({
		'nvim-telescope/telescope.nvim',
		requires = { { 'nvim-lua/plenary.nvim' } },
	})

	-- LSP

	-- Package manager
	use({ 'williamboman/mason.nvim' })

	use('williamboman/mason-lspconfig.nvim')

	use('neovim/nvim-lspconfig') -- Collection of configurations for the built-in LSP client

	use({
		'hrsh7th/nvim-cmp',
		'hrsh7th/cmp-buffer',
		'hrsh7th/cmp-path',
		'hrsh7th/cmp-nvim-lua',
		'hrsh7th/cmp-nvim-lsp',
		'hrsh7th/cmp-nvim-lsp-document-symbol',
	})

	-- formatting
	use({
		'jose-elias-alvarez/null-ls.nvim',
		config = function()
			require('plugins._null-ls')
			require('null-ls').setup()
		end,
		requires = { 'nvim-lua/plenary.nvim' },
	})

	-- Themes

	use({ 'briones-gabriel/darcula-solid.nvim', requires = 'rktjmp/lush.nvim' })
	-- use({ 'dracula/vim', as = 'dracula' })
	use({ 'ellisonleao/gruvbox.nvim' })
	use('folke/tokyonight.nvim')
	use({ 'shaunsingh/oxocarbon.nvim', run = './install.sh' })
	-- use({ 'catppuccin/nvim', as = 'catppuccin' })
	use({
		'norcalli/nvim-base16.lua',
		requires = { { 'norcalli/nvim.lua' } },
	})

	-- Other

	use('ollykel/v-vim') -- support for vlang
	use('tpope/vim-commentary') -- for commenting stuff with gc
	-- use('ThePrimeagen/git-worktree.nvim') -- for C-t
	use({
		'windwp/nvim-autopairs',
		config = function()
			require('nvim-autopairs').setup({})
		end,
	})

	use({
		'folke/which-key.nvim',
		config = function()
			require('which-key').setup({
				-- your configuration comes here
				-- or leave it empty to use the default settings
			})
		end,
	})

	-- Notes
	use('ixru/nvim-markdown')

	-- use({ 'iamcco/markdown-preview.nvim', run = 'cd app && yarn install', cmd = 'MarkdownPreview' }) -- Plugins can have post-install/update hooks
	-- use({ 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end, }) -- Use neovim in the browser

	if packer_bootstrap then
		require('packer').sync()
	end
end)
