-- null-ls config (formatting)
-- local null_ls_status_ok, null_ls = pcall(require('null-ls'))
-- if not null_ls_status_ok then
-- 	print('null-ls failed to load')
-- 	return
-- end
local null_ls = require('null-ls')

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics

null_ls.setup({
	sources = {
		formatting.clang_format,
		formatting.stylua,
		formatting.prettier,
		formatting.rustfmt,

		diagnostics.eslint,
		null_ls.builtins.completion.spell,
	},
})
