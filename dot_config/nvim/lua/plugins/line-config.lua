-- stylua: ignore

-- local colors = require('theme.config.theme').colors

-- local bubbles_theme = {
-- 	normal = {
-- 		a = { fg = colors.black, bg = colors.blue },
-- 		b = { fg = colors.white, bg = colors.grey },
-- 		c = { fg = colors.grey, bg = colors.black },
-- 	},

-- 	insert = { a = { fg = colors.black, bg = colors.cyan } },
-- 	visual = { a = { fg = colors.black, bg = colors.yellow } },
-- 	replace = { a = { fg = colors.black, bg = colors.red } },

-- 	inactive = {
-- 		a = { fg = colors.white, bg = colors.black },
-- 		b = { fg = colors.white, bg = colors.black },
-- 		c = { fg = colors.black, bg = colors.black },
-- 	},
-- }

require('lualine').setup({
	options = {
		icons_enabled = true,
		-- theme = bubbles_theme,
		component_separators = { left = '|', right = '|' },
		section_separators = { left = ' ', right = ' ' },
		disabled_filetypes = {},
		always_divide_middle = true,
		globalstatus = false,
	},
	sections = {
		lualine_a = { 'mode' },
		lualine_b = { 'branch', 'diff', 'diagnostics' },
		lualine_c = { { 'filename', file_status = false, path = 1 } },
		lualine_x = { 'encoding', 'fileformat', 'filetype' },
		lualine_y = { 'progress' },
		lualine_z = { 'location' },
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { 'filename' },
		lualine_x = { 'location' },
		lualine_y = {},
		lualine_z = {},
	},
	tabline = {},
	extensions = {},
})
