vim.g.mapleader = ' '
vim.g.maplocalleader = ' '


-- jump between buffers with Alt-hjkl
vim.keymap.set('n', '<M-l>', '<C-W><C-L>', { noremap = true })
vim.keymap.set('n', '<M-h>', '<C-W><C-H>', { noremap = true })
vim.keymap.set('n', '<M-k>', '<C-W><C-K>', { noremap = true })
vim.keymap.set('n', '<M-j>', '<C-W><C-J>', { noremap = true })
-- buffer management
vim.keymap.set('n', '<leader>bk', '<cmd>bdelete<cr>')
vim.keymap.set('n', '<leader>bb', '<cmd>Telescope buffers<cr>')
vim.keymap.set('n', 'gp', '<cmd>bprevious<cr>')
vim.keymap.set('n', 'gn', '<cmd>bnext<cr>')
vim.keymap.set('n', 'ga', '<cmd>b#<cr>')
-- move buffers in bufferline
vim.keymap.set('n', '<leader>b<', '<cmd>BufferLineMovePrev<cr>')
vim.keymap.set('n', '<leader>b>', '<cmd>BufferLineMoveNext<cr>')

-- remap undo
vim.keymap.set('n', 'U', ':redo<cr>')

-- move on a line
vim.keymap.set({'n', 'v'}, 'gl', '$')
vim.keymap.set({'n', 'v'}, 'gh', '0')

vim.keymap.set('n', 'vs', '<cmd>vsplit<cr>')
vim.keymap.set('n', '<leader>bs', '<cmd>split<cr>')

vim.keymap.set('n', '<leader>y', '"+y')
vim.keymap.set('n', '<leader>p', '"+p')

local terminal = require("nvterm.terminal")
local toggle_modes = {'n', 't'}
local mappings = {
  { toggle_modes, '<A-H>', function () terminal.toggle('horizontal') end },
  { toggle_modes, '<A-V>', function () terminal.toggle('vertical') end },
  { toggle_modes, '<A-I>', function () terminal.toggle('float') end },
}
local opts = { noremap = true, silent = true }
for _, mapping in ipairs(mappings) do
  vim.keymap.set(mapping[1], mapping[2], mapping[3], opts)
end

-- vim.keymap.set('n', '<leader>t', ':belowright split<CR> :terminal<CR> :resize 20N<CR> i', { noremap = true, silent = true }) -- terminal
vim.api.nvim_set_keymap("t", "<Esc>", "<C-\\><C-n>", {noremap = true, silent = true}) -- get out of terminal mode with esc

vim.keymap.set('n', '<leader>tw', ':set wrap!<cr>')

vim.keymap.set('n', '<leader>ot', ':NvimTreeToggle<cr>')


-- Keymaps for better default experience
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- [[ Highlight on yank ]]
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})
